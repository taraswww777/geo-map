$(document).ready(function () {

	function getJSON(url) {
		let $res;
		$.getJSON({
			url: url,
			async: false
		}, (res) => {
			$res = res;
		});
		return $res;
	}


	ymaps.ready(MapYandexInit);
	let points = getJSON('/data/yandex.json');

	function MapYandexInit() {
		const centerMap = [50.280875966808, 127.53449276278];

		let myMap = new ymaps.Map('ya-map', {
			center: centerMap,
			zoom: 12,
			// https://tech.yandex.ru/maps/doc/jsapi/2.1-dev/ref/reference/control.Manager-docpage/
			controls: [
				'fullscreenControl',
				'geolocationControl',
				'routeEditor',
				'rulerControl',
				'typeSelector',
				'zoomControl'
			]

		}, {
			searchControlProvider: 'yandex#search'
		});
		/**
		 * Создадим кластеризатор, вызвав функцию-конструктор.
		 * Список всех опций доступен в документации.
		 * @see https://api.yandex.ru/maps/doc/jsapi/2.1/ref/reference/Clusterer.xml#constructor-summary
		 */
		clusterer = new ymaps.Clusterer({
			/**
			 * Через кластеризатор можно указать только стили кластеров,
			 * стили для меток нужно назначать каждой метке отдельно.
			 * @see https://api.yandex.ru/maps/doc/jsapi/2.1/ref/reference/option.presetStorage.xml
			 */
			preset: 'islands#invertedVioletClusterIcons',
			/**
			 * Ставим true, если хотим кластеризовать только точки с одинаковыми координатами.
			 */
			groupByCoordinates: false,
			/**
			 * Опции кластеров указываем в кластеризаторе с префиксом "cluster".
			 * @see https://api.yandex.ru/maps/doc/jsapi/2.1/ref/reference/ClusterPlacemark.xml
			 */
			clusterDisableClickZoom: true,
			clusterHideIconOnBalloonOpen: false,
			geoObjectHideIconOnBalloonOpen: false
		});

		/**
		 * Функция возвращает объект, содержащий данные метки.
		 * Поле данных clusterCaption будет отображено в списке геообъектов в балуне кластера.
		 * Поле balloonContentBody - источник данных для контента балуна.
		 * Оба поля поддерживают HTML-разметку.
		 * Список полей данных, которые используют стандартные макеты содержимого иконки метки
		 * и балуна геообъектов, можно посмотреть в документации.
		 * @see https://api.yandex.ru/maps/doc/jsapi/2.1/ref/reference/GeoObject.xml
		 */
		getPointData = function (id, img, name, address, type_name, phone, url) {
			return {
				balloonContentHeader: '',
				balloonContentBody:
				'<div class="balloon">' +
				'<div class="balloon-img-block"><img src="' + img + '" class="balloon-img"></div>' +
				'<div class="balloon-name"><a class="balloon-url" href="' + url + '">' + name + '</a></div>' +
				'<div class="balloon-address">' + address + '</div>' +
				'<div class="balloon-type_name">' + type_name + '</div>' +
				'<div class="balloon-phone">' + phone + '</div>' +
				'</div>',
				balloonContentFooter: '',
				clusterCaption: name
			};
		};
		/**
		 * Функция возвращает объект, содержащий опции метки.
		 * Все опции, которые поддерживают геообъекты, можно посмотреть в документации.
		 * @see https://api.yandex.ru/maps/doc/jsapi/2.1/ref/reference/GeoObject.xml
		 */
		getPointOptions = function () {
			return {
				preset: 'islands#violetIcon'
			};
		};

		let geoObjects = [];

		$.each(points, function (i, point) {
			/**
			 * Данные передаются вторым параметром в конструктор метки, опции - третьим.
			 * @see https://api.yandex.ru/maps/doc/jsapi/2.1/ref/reference/Placemark.xml#constructor-summary
			 */
			geoObjects[i] = new ymaps.Placemark(point.coords, getPointData(
				point.id,
				point.img,
				point.name,
				point.address,
				point.type_name,
				point.phone,
				point.url
			), getPointOptions());
		});


		/**
		 * Можно менять опции кластеризатора после создания.
		 */
		clusterer.options.set({
			gridSize: 80,
			clusterDisableClickZoom: true
		});

		/**
		 * В кластеризатор можно добавить javascript-массив меток (не геоколлекцию) или одну метку.
		 * @see https://api.yandex.ru/maps/doc/jsapi/2.1/ref/reference/Clusterer.xml#add
		 */
		clusterer.add(geoObjects);
		myMap.geoObjects.add(clusterer);

		/**
		 * Спозиционируем карту так, чтобы на ней были видны все объекты.
		 */

		myMap.setBounds(clusterer.getBounds(), {
			checkZoomRange: true
		});


	}

});